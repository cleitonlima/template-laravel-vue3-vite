import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    // <-- this object is added
    debug: false,
    port: 8000,
    cors: false,
    watch: {
      usePolling: true,
    },
    strictPort: true,
    withCredentials: true,
    hmr: {
      port: 8000,
    },
    proxy: {
      "/api": {
        target: process.env.VITE_VUE_BASE_URL_API,
        changeOrigin: true,
        secure: false,
        rewrite: (path) => path.replace("/api/", ""),
        configure: (proxy, _options) => {
          proxy.on("error", (err, _req, _res) => {});
          proxy.on("proxyReq", (proxyReq, req, _res) => {
            proxyReq.setHeader("Origin", "foobar");
          });
          proxy.on("proxyRes", (proxyRes, req, _res) => {});
        },
      },
    },
  },
  plugins: [vue()],
  resolve: {
    alias: { "@": path.resolve(__dirname, "./") },
  },
  devServer: {
    proxy: "http://localhost:8080",
  },
});
